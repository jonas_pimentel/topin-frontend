describe("interface", function() {
  it("successfully loads", function() {
    cy.visit("http://localhost:8080");
    cy.get("h5").should("contain", "What would you like to eat?");
    cy.get("button.btn-secondary").click();
    cy.get("input#input-name").type("Chicken Brasil");
    cy.get("input#input-category").type("Chicken");
    cy.get("input#input-area").type("Indian");
    cy.get("textarea#input-cooking").type("OK");
    cy.get("button#addNewItemForm").click();
    cy.get("button#closeItemForm").click();
    cy.get("input#searchFoodInput").type("Chicken Brasil");
  });
});


