import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      title: "App | TOPi - Full Stack Developer Test",
    },
  },
  {
    path: "/details",
    name: "Details  ",
    meta: {
      title: "About | TOPi - Full Stack Developer Test",
    },
    component: () =>
      import(/* webpackChunkName: "details" */ "../views/Details.vue"),
  },
];



const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title;
  }
  next();
});

export default router
