export default {
  computed: {
    rows() {
      return this.list.length;
    },
  },
  data() {
    return {
      perPage: 3,
      currentPage: 1,
      loading: false,
      mainProps: { width: 75, height: 75, class: "m1" },
      fields: [
        { key: "strMeal", label: "Name" },
        { key: "strCategory", label: "Category" },
        { key: "strArea", label: "Area" },
        { key: "strMealThumb", label: "Thumb" },
        { key: "show_details", label: "Cooking Instructions" },
      ],
    };
  },
};
