import Vue from "vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

// Import CSS
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

// Import Plugin
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);