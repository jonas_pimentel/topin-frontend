import api from "@/services/http";

// const params for search
const params = {
  getByName: "search.php?s=",
};

export const getByNameService = (name) => api.get(`${params.getByName}${name}`);