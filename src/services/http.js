import axios from 'axios';

/*
/* API Documentation - @jonaspimenteldev
/* https://www.themealdb.com/api.php 
*/


// set url
const baseURL = 'https://www.themealdb.com/api/json/v1/1';

// export http
const http = axios.create({
  baseURL,
  withCredentials: false,
});

// interceptors
http.interceptors.response.use(
  (response) => response.data,
  (error) => Promise.reject(error),
);

export default http;