import { getByNameService } from "@/services/api";

export const namespaced = true;

export const state = {
  list: [],
};

export const mutations = {
  SET_LIST(state, list) {
    state.list = list;
  },
  ADD_LIST (state, list) {
      // mutate state
      state.list.push(list);
  }

};

export const actions = {
  async getByName({ commit }, name) {
    return getByNameService(name).then((res) => commit("SET_LIST", res.meals));
  },

  async addNewItem({ commit }, form) {
    // console.log(form);
    return commit("ADD_LIST", form);
  },

  async cleanList({ commit }) {
    return commit("SET_LIST", []);
  },
  
};
