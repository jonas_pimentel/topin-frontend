import Vue from "vue";
import Vuex from "vuex";
import * as api from "@/store/modules/api/index";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: { api },
});