# TOPi

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Run your tests
```
npx cypress open
```
